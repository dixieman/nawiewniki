onIndicatorMove = function () {
    let radius = 133;   // distance from center circle to indicator circle
    let mouseX = event.clientX - svg1.getBoundingClientRect().left;
    let mouseY = event.clientY - svg1.getBoundingClientRect().top;

    let centerCX = parseFloat(centerCircle.getAttributeNS(null, 'cx'));
    let centerCY = parseFloat(centerCircle.getAttributeNS(null, 'cy'));

    let kat = Math.atan((centerCY - mouseY) / (centerCX - mouseX));
    if (mouseX > centerCX) kat = kat + Math.PI;

    let degreeses = kat * 180 / Math.PI + 90;

    if ((degreeses < 40) && (degreeses > 0)) degreeses = 40;
    else if((degreeses > 320) && (degreeses < 360)) degreeses = 320;
    else if((degreeses > 320) && (degreeses < 360)) degreeses = 320;
    else if((degreeses > 73) && (degreeses < 77)) degreeses = 75;
    else if((degreeses > 108) && (degreeses < 112)) degreeses = 110;
    else if((degreeses > 143) && (degreeses < 147)) degreeses = 145;
    else if((degreeses > 178) && (degreeses < 182)) degreeses = 180;
    else if((degreeses > 213) && (degreeses < 217)) degreeses = 215;
    else if((degreeses > 248) && (degreeses < 252)) degreeses = 250;
    else if((degreeses > 283) && (degreeses < 287)) degreeses = 285;

    kat = (degreeses -90) * Math.PI /180;

    let newX = Math.cos(kat + Math.PI) * radius + centerCX;
    let newY = Math.sin(kat + Math.PI) * radius + centerCY;
    let airFlowValue = parseInt((degreeses - 40) * 1600 / 280);

    velocityText.textContent = airFlowValue;
    velocityText.setAttributeNS(null, 'x', newX);
    velocityText.setAttributeNS(null, 'y', newY + 3);
    smallCircle.setAttributeNS(null, 'cx', newX);
    smallCircle.setAttributeNS(null, 'cy', newY);
    manoArrow.setAttributeNS(null, "transform", "rotate( " + degreeses + " , " + 150 + " , " + 150 + ")");

    airFlow.value = airFlowValue;
}

manoIdicator.addEventListener("mousedown", function () {
    document.addEventListener('mousemove', onIndicatorMove);
    document.onmouseup = function () {
        document.removeEventListener('mousemove', onIndicatorMove);
        manoIdicator.onmouseup = null;
    };
});